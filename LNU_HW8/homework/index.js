// Your code goes here
let isEquals = function () {
    let x = prompt('Enter first operator: ');
    let y = prompt('Enter second operator: ');
    return x === y;
}

let numberToString = function () {
    let data = Number(prompt('Enter number: '));
    return data.toString();
}

let storeNames = function () {
    let str = prompt('Enter string:');
    let arr = [];
    arr = str.split(',');
    return arr;
}

let getDivision = function () {
    let firstNum = Number(prompt('Enter number: '));
    let secondNum = Number(prompt('Enter number: '));
    function isBigger() {
        if (secondNum > firstNum) {
            return secondNum / firstNum;
        } else {
            return firstNum / secondNum;
        }
    }
    let result = isBigger();
    if (result >= 1) {
        return result;
    } else {
        return 'Value is too small';
    }
}

let negativeCount = function () {
    let arr = [];
    let edje = 5; // avoiding "no-magic-numbers" warning :)
    for (let i = 0; i < edje; i++) {
        arr.push(prompt('Enter numbers ' + (i + 1)));
    }
    arr.join(', ');
    function getNegativeNumbers(array) {
        return array.filter(value => value < 0);
    }
    let arr1 = arr.map(Number);
    return getNegativeNumbers(arr1);
}

//Pay attention!
//just call function in console like:
// "some_function()" and input your data in the openning window